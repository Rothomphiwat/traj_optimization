import numpy as np
from numpy import linalg as LA


if __name__ == "__main__":

    r_end = np.array([0.0,0.1])
    r_start = np.array([0.0,0.0])
    t_start = 0
    t_end = 10

    t = (r_end-r_start)/LA.norm((r_end-r_start))

    # from poly at any timestamp
    r_any = np.array([0.0,2.0])

    delta = (r_any - r_start) - np.dot( (r_any - r_start), t ) * t

    print(delta)