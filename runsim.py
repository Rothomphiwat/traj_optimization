import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as Axes3D
from psutil import POSIX

from PathPlanning import RRTStar, Map
from TrajGen import trajGenerator, Helix_waypoints, Circle_waypoints
from Quadrotor import QuadSim
import controller
np.random.seed(8)

# 3D boxes   lx, ly, lz, hx, hy, hz
obstacles = [[-5, 25, 0, 20, 35, 60],
             [30, 25, 0, 55, 35, 100],
             [45, 35, 0, 55, 60, 60],
             [45, 75, 0, 55, 85, 100],
             [-5, 65, 0, 30, 70, 100],
             [70, 50, 0, 80, 80, 100]]

# limits on map dimensions
bounds = np.array([0,100])
# # create map with obstacles
mapobs = Map(obstacles, bounds, dim = 3)

# #plan a path from start to goal
start = np.array([80,20,10])
goal = np.array([10,50,10])

rrt = RRTStar(start = start, goal = goal,
              Map = mapobs, max_iter = 500,
              goal_sample_rate = 0.1)

# waypoints, min_cost = rrt.plan()


#scale the waypoints to real dimensions
waypoints = [ [ 0, 1, 2],
              [ 1, 2, 2],
              [ 1.3, 2, 2],
              [ 1.7, 2, 2],
              [ 2, 2, 2],
              [ 2.3, 2, 2],
              [ 2.7, 2, 2],
              [ 3, 2, 2],]


# waypoints = np.array(waypoints)*0.02

waypoints = np.array(waypoints)
#Generate trajectory through waypoints
traj = trajGenerator(waypoints, max_vel = 2, gamma = 1e6)
# alpha = 2.0/1.45175724
alpha = 1

#initialise simulation with given controller and trajectory
Tmax = traj.TS[-1]*alpha
print(Tmax)
print(traj.TS*alpha)
des_state = traj.get_des_state
sim = QuadSim(controller,des_state,Tmax,alpha = alpha)

t = np.linspace(0,Tmax,200)
pos = []
vel = []

for i in t:
    state = des_state(i,alpha)
    pos.append(state.pos)
    vel.append(state.vel)
pos = np.array(pos)
vel = np.array(vel)

#create a figure
fig = plt.figure()
ax = Axes3D.Axes3D(fig)
ax.set_xlim((0,3))
ax.set_ylim((0,3))
ax.set_zlim((0,3))

ax.plot3D(pos[:,0],pos[:,1],pos[:,2])
#plot the waypoints and obstacles
rrt.draw_path(ax, waypoints)
# mapobs.plotobs(ax, scale = 0.02)
# plt.show()
#run simulation
fig2 = plt.figure(2)
# plt.plot(pos[:,0],pos[:,1])
plt.plot(t,vel[:,1])

sim.run(ax)
